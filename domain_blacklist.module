<?php
/**
 * @file
 * Domain Blacklist module file.
 */

/**
 * Implements hook_menu().
 */
function domain_blacklist_menu() {
    $items['admin/config/system/domain_blacklist'] = array(
        'title' => 'Domain blacklist',
        'description' => 'Configure domain blacklisting',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('domain_blacklist_admin_form'),
        'access arguments' => array('administer domain blacklist'),
    );
    return $items;
}

/**
* Implements hook_permission().
*/
function domain_blacklist_permission() {
    return array(
        'administer domain blacklist' => array(
            'title' => t('Administer domain blacklist'),
            'description' => t('Allows a user to administer the domain blacklist settings.'),
        ),
    );
}

/**
* Administration form for domain blacklist.
*/
function domain_blacklist_admin_form($form, &$form_state) {
    $form['domain_blacklist_count'] = array(
        '#type' => 'markup',
        '#markup' => "<p><b>" . t('Blocked registration attempts: ') . "</b>" . variable_get("domain_blacklist_count", 0) . "</p>"
    );
    $form['domain_blacklist'] = array(
        '#type' => 'textarea',
        '#required' => TRUE,
        '#title' => t('Blacklist service'),
        '#default_value' => variable_get('domain_blacklist'),
        '#description' => t('Enter the domain of your chosen blacklisting services, one per line.'),
    );
    $form['domain_blacklist_domain_message'] = array(
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => t('Error message'),
        '#default_value' => variable_get('domain_blacklist_domain_message', t('You domain has been blacklisted by %s.')),
        '#description' => t('Enter the error message you want the user to see if the domain is found on a blacklist. %s is replaced by the domain of the service it was found on.'),
    );
    $form['domain_blacklist_email_message'] = array(
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => t('Error message'),
        '#default_value' => variable_get('domain_blacklist_email_message', t('You email address has been blacklisted by %s.')),
        '#description' => t('Enter the error message you want the user to see if the email address is found on a blacklist. %s is replaced by the domain of the service it was found on.'),
    );
    return system_settings_form($form);
}

/**
* Implements hook_form_form_id_form_alter().
*/
function domain_blacklist_form_user_register_form_alter(&$form, &$form_state, $form_id) {
    $form['#validate'][] = 'domain_blacklist_user_register_validate';
}

/**
* Custom validation function.
*
* Checks if the domain in the email address is on a list of allowed domains.
*/
function domain_blacklist_user_register_validate(&$form, &$form_state) {
    // Ignore validation if mail already has an error.
    $errors = form_get_errors();
    if (!empty($errors['mail'])) {
        return;
    }
    $domain_message = variable_get("domain_blacklist_domain_message", t('You domain has been blacklisted by %s.'));
    $email_message = variable_get("domain_blacklist_email_message", t('You email has been blacklisted by %s.'));
    $mail = explode('@', strtolower($form_state['values']['mail']));
    $services = variable_get('domain_blacklist', array());

    $localpart = $mail[0];
    $domain = $mail[1];

    $seps = explode("+", $localpart);
    $localpart = $seps[0];
    $localpart = str_replace(".", "", $localpart);


    if (!empty($services)) {

        $services = explode("\r\n", $services);

        foreach ($services as $service) {

            $testdomain = "$domain.$service";

            $record = dns_get_record($testdomain, DNS_A);
            if ($record !== false) {
                if (count($record) > 0) {
                    $ip = explode(".", $record[0]['ip']);
                    if (($ip[0] == "127") && ($ip[1] == "0")) {
                        $count = variable_get("domain_blacklist_count", 0);
                        $count++;
                        variable_set("domain_blacklist_count", $count);
                        $message = str_replace("%s", $service, $domain_message);
                        form_set_error('account', $message);
                        break;
                    }
                }
            }

            $testdomain = "$localpart.at.$domain.$service";

            $record = dns_get_record($testdomain, DNS_A);
            if ($record !== false) {
                if (count($record) > 0) {
                    $ip = explode(".", $record[0]['ip']);
                    if (($ip[0] == "127") && ($ip[1] == "0")) {
                        $count = variable_get("domain_blacklist_count", 0);
                        $count++;
                        variable_set("domain_blacklist_count", $count);
                        $message = str_replace("%s", $service, $email_message);
                        form_set_error('account', $message);
                        break;
                    }
                }
            }


        }
    }

}
