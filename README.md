Domain Blacklist is a system to prevent registration in Drupal 7 by spammers by using
a DNS Blacklist type service for domains.  This works like DNS Blacklists for IP addresses
but instead of IP addresses domains are looked up instead.

This module is based largely upon "Domain Registration" by Arvinsingla.

To use it you simply install and enable the module. Then go to the configuration page
and enter a list of DBS Domain Blacklist services to query.  When a user tries to register
the domain name of their email address is then checked against each of these services
in order and if a result is found the registration is denied.

At the moment the only known supported DNS blacklist is antispam.majenko.co.uk
